variables:
  ## This value MUST be the same as `opam_repository_tag` in `scripts/version.sh`
  build_deps_image_version: 6aeb7cb2edc4b876fb206751e852fd071836b520
  build_deps_image_name: registry.gitlab.com/tezos/opam-repository
  public_docker_image_name: docker.io/${CI_PROJECT_PATH}-
  GIT_STRATEGY: fetch
  GIT_DEPTH: "1"
  GET_SOURCES_ATTEMPTS: "2"
  ARTIFACT_DOWNLOAD_ATTEMPTS: "2"
  # Sets the number of tries before failing opam downloads.
  OPAMRETRIES: "5"

# Basic, specialised, minimal, orthogonal templates

# Some settings we want by default on all jobs
.default_settings_template:
  interruptible: true

# Image templates
.image_template__runtime_build_test_dependencies_template:
  image: ${build_deps_image_name}:runtime-build-test-dependencies--${build_deps_image_version}

.image_template__runtime_build_dependencies_template:
  image: ${build_deps_image_name}:runtime-build-dependencies--${build_deps_image_version}

.image_template__latest:
  image: docker:latest

# Rules template

# Rules for all the jobs that need to be run on development branches (typically
# those that have an MR, but also some experiments, etc.)
.rules_template__development:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_TAG != null && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - when: on_success

# Same as .rules_template__development, but for manual jobs.
.rules_template__development_manual:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_TAG != null && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - when: manual

# Rules for all the jobs that are run only for the master branch and the like
# (tags, releases, etc.)
.rules_template__master_and_releases:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE"'
      when: never
    - if: '$CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: on_success
    - if: '$CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: on_success
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: on_success
    - when: never

# Rules for all the jobs that are run only for the master branch
.rules_template__master:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: on_success
    - when: never

# Rules for specific topics: doc, opam, etc.
.rules_template__development_documentation:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE" && $TZ_SCHEDULE_KIND == "EXTENDED_TESTS"'
      when: always
    - if: '$CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /doc/'
      when: always
    - changes:
        - /docs
      when: always
    - when: never
.rules_template__development_opam:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE" && $TZ_SCHEDULE_KIND == "EXTENDED_TESTS"'
      when: always
    - if: '$CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /-release$/ && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "arvidnl"'
      when: always
    - if: '$CI_COMMIT_BRANCH =~ /opam/'
      when: always
    - if: '$TZ_OPAM_FILES_MODIFIED == "true"'
      when: always
    - when: never

.rules_template__development_coverage:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE" && $TZ_SCHEDULE_KIND == "EXTENDED_TESTS"'
      when: always
    - if: '$CI_COMMIT_BRANCH =~ /coverage/'
      when: always
    - when: manual
      allow_failure: true

.rules_template__extended_test_pipeline:
  rules:
    - if: '$TZ_PIPELINE_KIND == "SCHEDULE" && $TZ_SCHEDULE_KIND == "EXTENDED_TESTS"'
      when: always
    - when: never
